# opencv_calibrate

Reference: [OpenCV-Python Tutorials: Camera Calibration and 3D Reconstruction](http://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_calib3d/py_calibration/py_calibration.html)

## Note

The `opencv-python` package from PyPi installed with `pip` is very limited and can not be used. `opencv-3.3.0` compiled with python3 support is used.
