#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" cli.py

CLI interface for opencv_calibrate.

"""

from __future__ import division, print_function, absolute_import

import os
import argparse
import sys
import logging
import time

from calibrate import Board, Camera

__author__ = "Derek Goddeau"

_logger = logging.getLogger(__name__)


def main(args):
    """ Setup logs, gather args, and start the crawl.

    """
    setup_logs()
    _logger.debug("Starting main()")

    args = parse_args(args)

    # Abort if no training data
    if not os.path.exists(args.image_directory):
        if args.capture:
            os.makedirs(args.image_directory)
        else:
            print("[*] Error: Image directory does not exist")
            sys.exit(1)

    board = Board(args.width, args.height, args.pattern_type)
    camera = Camera(board, args.image_directory)

    if args.capture:
        camera.capture(50, args.delay)

    camera.process_images()
    camera.calibrate()
    camera.save_calibration()

    _logger.debug("All done, shutting down.")
    logging.shutdown()


def parse_args(args):
    """ Parse command line parameters.

    :return: command line parameters as :obj:`airgparse.Namespace`
    Args:
        args ([str]): List of strings representing the command line arguments.

    Returns:
        argparse.Namespace: Simple object with a readable string
        representation of the argument list.

    """
    parser = argparse.ArgumentParser(
        description="Calibrate a camera with OpenCV.")

    parser.add_argument(
        '-v',
        '--video-input',
        type=str,
        dest='video_input',
        default='/dev/video0',
        help="Source of input video",
        action='store')
    parser.add_argument(
        '-c',
        '--capture',
        help="Capture calibration images",
        action='store_true')
    parser.add_argument(
        '-i',
        '--image-directory',
        type=str,
        dest='image_directory',
        help="Directory with images for calibration",
        action='store')
    parser.add_argument(
        '-p',
        '--pattern-type',
        type=str,
        dest='pattern_type',
        default='chessboard',
        choices=['circles', 'chessboard', 'dual_circles', 'ch_aruco'],
        help="Pattern type for calibration",
        action='store')
    parser.add_argument(
        '-s',
        '--size',
        type=float,
        help="Distance between two nearest centers of circles or squares on calibration board",
        action='store')
    parser.add_argument(
        '--width',
        type=int,
        default=9,
        dest='width',
        help="width of pattern (in corners or circles)",
        action='store')
    parser.add_argument(
        '--height',
        type=int,
        default=6,
        dest='height',
        help="height of pattern (in corners or circles)",
        action='store')
    parser.add_argument(
        '-o',
        '--output-file',
        type=str,
        dest='output_file',
        default='camera_parameters.yaml',
        help="Output file name",
        action='store')
    parser.add_argument(
        '-d',
        '--delay',
        type=float,
        default=1.5,
        help="delay between captures in seconds",
        action='store')
    parser.add_argument(
        '--config',
        type=str,
        help="Config file with parameters to load",
        action='store')

    return parser.parse_args(args)


def setup_logs():
    """ Set up logger to be used between all modules.

    Set logging root and file handler configuration to default to
    ``DEBUG`` and write output to ``main.log``. Set console
    handler to default to ``ERROR``.

    """
    logging.basicConfig(level=logging.DEBUG, filename='../calibration.log',
                        filemode='w')
    _logger.setLevel(logging.DEBUG)

    # create file handler which logs messages
    fh = logging.FileHandler('../calibration.log')
    fh.setLevel(logging.DEBUG)

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)

    # add the handlers to the logger
    _logger.addHandler(fh)
    _logger.addHandler(ch)


if __name__ == "__main__":

    start = time.time()
    main(sys.argv[1:])
    print("[*] Calibrated in {} seconds".format(time.time() - start))
    sys.exit(0)
