#!/bin/env python
"""calibrate.py

"""

from __future__ import print_function, division, absolute_import

import glob
import time
import numpy as np
import cv2


class Camera:
    """A camera for calibartion.

    Args:
        board: A ``Board`` object.
        image_directory(str): The directory the training images are located.
        max_iter(int): An integer specifying maximum number of iterations.
        epsilon(float): Required accuracy.

    Attributes:
        board: A ``Board`` object.
        criteria(tuple(type, max_iter, epsilon)): The iteration termination
            criteria. When this criteria is satisfied, algorithm iteration
            stops. Actually, it should be a tuple of 3 parameters.

    """
    def __init__(self, board, image_directory, out_file="calibration.yml", max_iter=30, epsilon=0.001):

        self.board = board

        # termination criteria
        self.criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, max_iter, epsilon)

        # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
        objp = np.zeros((board.height*board.width, 3), np.float32)
        objp[:, :2] = np.mgrid[0:board.width, 0:board.height].T.reshape(-1, 2)
        self.objp = objp

        # Arrays to store object points and image points from all the images.
        self.objpoints = [] # 3d point in real world space
        self.imgpoints = [] # 2d points in image plane.

        self.memory_card = MemoryCard(image_directory, out_file)
        self.image_directory = image_directory

        # Camera calibration output
        self.camera_matrix = None
        self.distortion_matrix = None
        self.rotation_vectors = None
        self.translation_vectors = None

    def process_images(self):
        """Read images of chessboard to find patterns for calibration.

        """
        for image in self.memory_card.images:
            image.find_pattern(self.board.width, self.board.height)

            # If found, add object points, image points (after refining them)
            if image.found_pattern:
                self.objpoints.append(self.objp)

                refined_corners = cv2.cornerSubPix(image.image,
                                                   image.pattern_corners,
                                                   (11,11),
                                                   (-1,-1),
                                                   self.criteria)

                self.imgpoints.append(refined_corners)

                # Draw and display the corners
                processed_image = cv2.drawChessboardCorners(
                    image.image, (self.board.width, self.board.height), refined_corners, image.found_pattern)
                cv2.imshow('img', processed_image)
                cv2.waitKey(500)

        cv2.destroyAllWindows()

    def calibrate(self):
        """Calibrate camera.

        """
        print("[*] Calibrating")
        success, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(
            self.objpoints, self.imgpoints, self.memory_card.images[0].image.shape[::-1], None, None)

        if success:
            print("[*] Calibration successful")
            self.camera_matrix = mtx
            self.distortion_matrix = dist
            self.rotation_vectors = rvecs       # list for all images
            self.translation_vectors = tvecs    # list for all images
        else:
            print("[*] Calibration failed")

    def reprojection_error(self):
        """Calculate how exect calibration is.

        Should be as close to zero as possible.

        """
        print("[*] Calculating error")
        mean_error = 0
        total_error = 0
        for i in range(len(self.objpoints)):
            imgpoints2, _ = cv2.projectPoints(self.objpoints[i],
                                              self.rotation_vectors[i],
                                              self.translation_vectors[i],
                                              self.camera_matrix,
                                              self.distortion_matrix)
            error = cv2.norm(self.imgpoints[i], imgpoints2, cv2.NORM_L2) / len(imgpoints2)
            total_error += error

        print("Mean error: {}".format(total_error / len(self.objpoints)))
        print("Total error: {}".format(total_error))

    def capture(self, num_images, delay):
        """Capture images and save to file.

        """
        cap = cv2.VideoCapture(0)

        image_num = 0
        while image_num < num_images:

            success, frame = cap.read()
            cv2.imshow('capture', frame)

            if not success:
                print("[*] Error: capture failed")

            k = cv2.waitKey(1)

            if k % 256 == 32: # space is pressed
                cv2.imwrite(self.image_directory + '/' + str(image_num) + '.png', frame)
                print("[*] Info: Saved image {}".format(image_num))
                image_num += 1

            time.sleep(delay)

        cap.release()
        cv2.destroyAllWindows()
        self.memory_card.load_images()

    def save_calibration(self):
        """Save the camera calibration to OpenCV YAML.

        """
        self.reprojection_error()
        print("[*] Saving calibration")
        self.out_file = cv2.FileStorage('data.yml', flags=1)
        self.out_file.write(name='camera_matrix', val=self.camera_matrix)
        self.out_file.write(name='distortion_matrix', val=self.distortion_matrix)
        self.out_file.release()


class MemoryCard:
    """Memory management.

    Container for images and handles saveing and reading data.

    """
    def __init__(self, image_directory, out_file):
        # Image file paths
        image_paths = glob.glob(image_directory + '/' + '*.png')
        self.image_directory = image_directory
        self.images = []
        for image_path in image_paths:
            self.images.append(Image(image_path))

    def load_images(self):
        """Load images from a directory.

        """
        for image_path in glob.glob(self.image_directory + '*.png'):
            self.images.append(Image(image_path))


class Image:
    """Images from the camera to use for calibration.

    """
    def __init__(self, file_name):
        self.raw_image = cv2.imread(file_name)
        self.gray_image = cv2.cvtColor(self.raw_image, cv2.COLOR_BGR2GRAY)
        self.found_pattern = False
        self.pattern_corners = None

    @property
    def image(self):
        """This is the image that will be used for analysis.

        """
        return self.gray_image

    def find_pattern(self, board_width, board_height):
        """Find the corners of the pattern.

        """
        success, corners = cv2.findChessboardCorners(
            self.image, (board_width, board_height), None)
        if success:
            self.found_pattern = True
            self.pattern_corners = corners


class Board:
    """A board for camera calibration.

    Defaults are set for the included chessboard image that is included.

    Args:
        pattern(str): Pattern type for calibration.
        width(int): Width of pattern (in corners or circles).
        height(int): Height of pattern (in corners or circles).

    Attributes:
        pattern(str): Pattern type for calibration.
        width(int): Width of pattern (in corners or circles).
        height(int): Height of pattern (in corners or circles).

    """
    def __init__(self, width=9, height=6, pattern='chessboard'):
        self.pattern = pattern
        self.width = width
        self.height = height
