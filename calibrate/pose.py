#!/bin/env python

import cv2
import numpy as np
import glob


def draw_axis(image, corners, image_points):
    """Draw 3D axis on a corner of the board.

    """
    corner = tuple(corners[0].ravel())
    image = cv2.line(image, corner, tuple(image_points[0].ravel()), (255,0,0), 5)
    image = cv2.line(image, corner, tuple(image_points[1].ravel()), (0,255,0), 5)
    image = cv2.line(image, corner, tuple(image_points[2].ravel()), (0,0,255), 5)
    return image

def draw_cube(image, corners, image_points):
    """Draw a cube on a corner of the board.

    """
    image_points = np.int32(image_points).reshape(-1,2)

    # draw ground floor in green
    image = cv2.drawContours(image, [image_points[:4]], -1, (0, 255, 0), -3)

    # draw pillars in blue color
    for i, j in zip(range(4), range(4, 8)):
        image = cv2.line(image, tuple(image_points[i]), tuple(image_points[j]), (255), 3)

        # draw top layer in red color
        image = cv2.drawContours(image, [image_points[4:]], -1, (0, 0, 255), 3)

    return image

if __name__ == '__main__':

    # load camera calibration
    data = cv2.FileStorage('../data/logitech_c615.yml', cv2.FILE_STORAGE_READ)
    camera_matrix = data.getNode('camera_matrix').mat()
    distortion_matrix = data.getNode('distortion_matrix').mat()
    data.release()

    # Create termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    objp = np.zeros((6*9,3), np.float32)
    objp[:,:2] = np.mgrid[0:9,0:6].T.reshape(-1,2)

    # For draw_axis()
    # axis = np.float32([[3,0,0], [0,3,0], [0,0,-3]]).reshape(-1,3)

    # For draw_cube()
    axis = np.float32([[0,0,0], [0,3,0], [3,3,0], [3,0,0],
                        [0,0,-3],[0,3,-3],[3,3,-3],[3,0,-3] ])

    for fname in glob.glob('../test/logitech_c615/*.png'):
        image = cv2.imread(fname)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        ret, corners = cv2.findChessboardCorners(gray, (9,6),None)

        if ret == True:
            corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)

            # Find the rotation and translation
            # vectors.
            _, rvecs, tvecs, inliers = cv2.solvePnPRansac(objp, corners2, camera_matrix, distortion_matrix)

            # project 3D points
            # to image plane
            image_points, jac = cv2.projectPoints(axis, rvecs, tvecs, camera_matrix, distortion_matrix)

            image = draw_cube(image, corners2, image_points)
            cv2.imshow('img', image)
            k = cv2.waitKey(0) & 0xff
            if k == 's':
                cv2.imwrite(fname[:6]+'.png', image)

    cv2.destroyAllWindows()
